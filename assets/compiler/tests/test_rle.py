import pytest
from compiler.rle import rle


@pytest.mark.parametrize(
    "input, output",
    [
        ((), (0,)),
        (
            (1, 2, 3, 4),
            (1, 1, 1, 2, 1, 3, 1, 4, 0),
        ),
        (
            (1, 1, 1, 1),
            (4, 1, 0),
        ),
        (
            (1,) * 300,
            (255, 1, 45, 1, 0),
        ),
    ],
)
def test_rle(input, output):
    assert list(rle(input)) == list(output)
