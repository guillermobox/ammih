from PIL import Image


class WrongImage(Exception):
    pass


def image_as_tiles(path, colormap):
    img = Image.open(path)
    palette = extract_color_palette(img, colormap)
    return split_image(img), palette


def extract_color_palette(img: Image.Image, colormap):
    if img.format != "PNG":
        raise WrongImage(f"not a png, but {img.format}")
    colors = img.getcolors()
    if len(colors) > 4:
        raise WrongImage(f"too many colors, {len(colors)} > 4")

    if img.mode == "P":
        return colormap

    palette = dict()
    for count, color in colors:
        if color[3] == 255:
            asint = (color[0] << 16) + (color[1] << 8) + color[2]
        else:
            asint = None

        if asint is None:
            palette[color] = 0
        elif asint in colormap:
            palette[color] = colormap[asint]
        else:
            raise WrongImage(f"unknown color, {count} pixels of {color}")

    return palette


def split_image(img: Image.Image):
    if img.width % 8 != 0:
        raise WrongImage(f"width not a multiple of 8, but {img.width}")
    if img.height % 8 != 0:
        raise WrongImage(f"height not a multiple of 8, but {img.height}")

    rows = img.height // 8
    cols = img.width // 8

    for row in range(rows):
        for col in range(cols):
            box = (col * 8, row * 8, (col + 1) * 8, (row + 1) * 8)
            crop = img.crop(box)
            yield row, col, crop
