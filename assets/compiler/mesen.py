import struct
from pathlib import Path

from PIL import Image, ImageDraw


def color_distance(a, b):
    return sum(abs(a[i] - b[i]) for i in range(3))


def mesen_palette_match(color):
    closest = sorted(MESEN_PALETTE, key=lambda col: color_distance(col, color))[0]
    exact = closest == color
    return exact, MESEN_PALETTE.index(closest)


def color_to_string(color):
    if color[3] == 0:
        return "transparent"
    else:
        exact, index = mesen_palette_match(color)
        return (
            f"0x{color[0]:02X}{color[1]:02X}{color[2]:02X}"
            f"(palette {index:02X} {'exact' if exact else 'similar'})"
        )


with open(Path(__file__).parent / "mesen.pal", "rb") as fh:
    MESEN_PALETTE = list(struct.iter_unpack("BBB", fh.read()))

rows, cols = 4, 16
cellsize = 32

img = Image.new("RGB", (cols * cellsize, rows * cellsize))
ctx = ImageDraw.Draw(img)

for row in range(rows):
    for col in range(cols):
        idx = row * cols + col
        x0, y0 = col * cellsize, row * cellsize
        x1, y1 = x0 + cellsize, y0 + cellsize
        ctx.rectangle((x0, y0, x1, y1), fill=MESEN_PALETTE[idx])

img.save("mesen.png")
