import sys


def print_address_table(label, addresses, linewidth=30, file=sys.stdout):
    print(f"{label}:")
    line = []
    for address in addresses:
        line.append(address)
        if sum(map(len, line)) > linewidth:
            print(f".addr {','.join(line)}", file=file)
            line = []
    if line:
        print(f".addr {','.join(line)}", file=file)
    print()


def print_data(label, data, width=8, file=sys.stdout):
    print(f"{label}:", file=file)
    while data:
        print(".byte " + ",".join(f"${x:02X}" for x in data[0:width]), file=file)
        data = data[width:]


def print_symbols(symbols, file=sys.stdout):
    maxlen = max(map(len, symbols.keys()))
    for symbol, value in symbols.items():
        print(f"{symbol:{maxlen}} = ${value:02x}", file=file)
