FROM alpine:3.18 as build

RUN apk add --no-cache gcc make musl-dev git
RUN git clone --single-branch --branch V2.19 --depth 1 https://github.com/cc65/cc65.git
RUN cd cc65; make ../bin/ca65 ../bin/ld65

FROM python:3.12-alpine3.18

COPY --from=build /cc65/bin/??65 /bin/
COPY --from=build /cc65/cfg/nes.cfg /opt/nes.cfg

RUN apk add --no-cache make
RUN pip install Pillow==10.1.0 PyYAML==6.0.1 pytest==7.4.3 ruff==0.1.3 --only-binary :all:

CMD sh
