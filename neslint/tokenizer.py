import re

_regexp = re.compile(
    r"""
    \s*(?P<label>[a-zA-Z@_]*:)?
    \s*
    (
         (?P<declaration>[a-zA-Z0-9_]+\s*=\s*[$%0-9a-zA-Z_+| ]*[^\s])
        |(?P<command>\.[a-z]+\s*[^;]*[^\s])
        |(?P<instruction>[a-zA-Z]{3}(\s[^;]*[^\s])?)
    )?
    \s*(?P<comment>;.*)?
    """,
    re.VERBOSE,
)


def tokenize(line):
    return _regexp.fullmatch(line)
