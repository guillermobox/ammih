import pytest

from neslint.tokenizer import tokenize


@pytest.mark.parametrize(
    "line, label",
    [
        ("a:", "a:"),
        ("  a:   ", "a:"),
        (":", ":"),
        ("   :   ", ":"),
        ("@here:", "@here:"),
        ("LabelName:", "LabelName:"),
        ("label: inx", "label:"),
        ("label: ; comment", "label:"),
        ("label: .byte $22", "label:"),
    ],
)
def test_tokenize_labels(line, label):
    assert tokenize(line)["label"] == label


@pytest.mark.parametrize(
    "line, instruction",
    [
        ("inx", "inx"),
        ("lda #$55", "lda #$55"),
        ("label: dex", "dex"),
        ("  inx  ", "inx"),
        ("  lda #$20  ;comment", "lda #$20"),
        ("label: beq @loop ;comment", "beq @loop"),
    ],
)
def test_tokenize_instructions(line, instruction):
    assert tokenize(line)["instruction"] == instruction


@pytest.mark.parametrize(
    "line, command",
    [
        (".load", ".load"),
        (".byte $12, $24", ".byte $12, $24"),
        ("begin: .macro", ".macro"),
        (".data ;comment", ".data"),
    ],
)
def test_tokenize_commands(line, command):
    assert tokenize(line)["command"] == command


@pytest.mark.parametrize(
    "line, declaration",
    [
        ("a = 2", "a = 2"),
        ("     A = $20", "A = $20"),
        ("A = 20 ; comment", "A = 20"),
    ],
)
def test_tokenize_declarations(line, declaration):
    assert tokenize(line)["declaration"] == declaration
