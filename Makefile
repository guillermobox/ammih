SOURCES := $(wildcard src/*.s)
ASSET_COMPILER := $(wildcard assets/compiler/*.py)
ASSETS := $(addprefix assets/,chr.bin stages.s chr.s messages.s stages.s)

export PYTHONPATH := $(CURDIR)/assets:$(CURDIR)

.PHONY: clean all assets check fix test

all: ammih.nes

test: $(ASSET_COMPILER)
	python -m pytest --junit-xml=junit.xml

check:
	ruff check --no-fix .
	ruff format --check .

fix:
	ruff check --fix .
	ruff format .

clean:
	rm -f ammih.nes ammih.dbg src/ammih.o
	rm -f $(ASSETS)
	find . -type f -name *.pyc -delete
	find . -type d -name __pycache__ -delete

ammih.nes: $(ASSETS) $(SOURCES)
	ca65 --debug-info src/ammih.s --include-dir .
	ld65 --dbgfile ammih.dbg --config /opt/nes.cfg src/ammih.o -o ammih.nes

$(ASSETS): $(ASSET_COMPILER)

assets/chr.s assets/chr.bin: assets/chr.yaml assets/*.png
	python -m compiler.chr $<

assets/messages.s: assets/messages.yaml
	python -m compiler.messages $< > $@

assets/stages.s: assets/stages.yaml
	python -m compiler.stages $< > $@
