# A Match Made In Heaven

This is a basic puzzle-like NES game. The latest builds are [available online](https://guillermobox.gitlab.io/ammih), and can be played or downloaded, to play offline.

The requisites to compile the game are: [6502 assembler](https://github.com/cc65/cc65.git), make, and python3 with some packages to compile the game assets. This can be setup as a Docker container with the provided Dockerfile.

Any code submitted to the repo should conform to the linting/formatting. This is done via `make check`. Some of the issues can be automatically fixed, for that `make fix` would do.

## The game

This is a puzzle game consisting on two characters that move synchronously. On each stage there are exit locations, and the objective is to put both characters at the same time in those locations.

## Gameplay

Just use the arrows to move both characters around.

## Development

I recommend using the emulator Mesen, as it has amazing debugging tools. I added a pelette in src/assets from that emulator, that can be used in pixel editors to create sprites or tiles with precise NTFS colors.

